package elog.utils;

import com.codahale.metrics.health.HealthCheck;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import elog.structures.Constants;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Utils {
    public static final ObjectMapper objectMapper = new ObjectMapper();

    private Utils() {
        // Static
    }

    public static boolean fileExists(String filePath) {
        File file = new File(filePath);
        return (file.exists() && !file.isDirectory());
    }

    public static Map<String, ObjectNode> collectEventDetails(String eventMetadataFolder) throws IOException {
        Logger log = LoggerFactory.getLogger(Utils.class);
        log.debug("Reading event-details files from folder: {}", eventMetadataFolder);
        Map<String, ObjectNode> idToEventDetails = new HashMap<>();
        File[] eventMetadataFiles = new File(eventMetadataFolder).listFiles((dir, name) -> name.toLowerCase(Locale.ROOT).endsWith(".json"));
        if (eventMetadataFiles != null) {
            log.debug("Found {} event-details files in {}.", eventMetadataFiles.length, eventMetadataFiles);
            for (var eventMetadataFile : eventMetadataFiles) {
                ObjectNode eventObjectNode = objectMapper.readValue(eventMetadataFile, ObjectNode.class);
                JsonNode eventIdNode = eventObjectNode.get(Constants.EVENT_ID_FIELD_NAME);
                if (eventIdNode == null) {
                    log.warn("Ignoring file {} since it doesn't contain a field named {}", eventMetadataFile.getName(), Constants.EVENT_ID_FIELD_NAME);
                    continue;
                }
                idToEventDetails.put(eventIdNode.asText(), eventObjectNode);
            }
        }

        return Collections.unmodifiableMap(idToEventDetails);
    }

    public static void printLogo() {
        String logo = "███████╗██╗   ██╗███████╗███╗   ██╗████████╗              ██╗      ██████╗  ██████╗ \n" +
                "██╔════╝██║   ██║██╔════╝████╗  ██║╚══██╔══╝              ██║     ██╔═══██╗██╔════╝ \n" +
                "█████╗  ██║   ██║█████╗  ██╔██╗ ██║   ██║       █████╗    ██║     ██║   ██║██║  ███╗\n" +
                "██╔══╝  ╚██╗ ██╔╝██╔══╝  ██║╚██╗██║   ██║       ╚════╝    ██║     ██║   ██║██║   ██║\n" +
                "███████╗ ╚████╔╝ ███████╗██║ ╚████║   ██║                 ███████╗╚██████╔╝╚██████╔╝\n" +
                "╚══════╝  ╚═══╝  ╚══════╝╚═╝  ╚═══╝   ╚═╝                 ╚══════╝ ╚═════╝  ╚═════╝ ";
        System.out.println(logo);
    }

    public static void registerDummyHealthCheck(Environment environment) {
        environment.healthChecks().register("dummy", new HealthCheck() {
            @Override
            protected Result check() {
                return Result.healthy();
            }
        });
    }
}
