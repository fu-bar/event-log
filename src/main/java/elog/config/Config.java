package elog.config;

import io.dropwizard.Configuration;

import java.util.Map;

public class Config extends Configuration {
    private String eventsDetailsFolder;
    private Map<String, String> db;

    public String getEventsDetailsFolder() {
        return eventsDetailsFolder;
    }

    public void setEventsDetailsFolder(String eventsDetailsFolder) {
        this.eventsDetailsFolder = eventsDetailsFolder;
    }

    public Map<String, String> getDb() {
        return db;
    }

    public void setDb(Map<String, String> db) {
        this.db = db;
    }
}
