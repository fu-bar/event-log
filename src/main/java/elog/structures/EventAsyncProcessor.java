package elog.structures;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import elog.interfaces.PersistenceDriver;
import elog.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class EventAsyncProcessor implements Runnable {

    private final Logger log;
    private static final Pattern templatePattern = Pattern.compile("<<(.*?)>>");
    private final BlockingQueue<JsonNode> eventQueue;
    private final Map<String, ObjectNode> eventIdToObjectNode;
    private final Set<PersistenceDriver> persistenceDrivers = new HashSet<>();
    private final Consumer<Throwable> exceptionHandler;
    private boolean isRunning = false;

    public EventAsyncProcessor(BlockingQueue<JsonNode> eventQueue, Map<String, ObjectNode> eventIdToObjectNode, Collection<PersistenceDriver> persistenceDrivers, Consumer<Throwable> exceptionHandler) {
        super();
        if (exceptionHandler == null) {
            throw new NullPointerException("Exception handler must not be null");
        }
        this.log = LoggerFactory.getLogger(EventAsyncProcessor.class);
        this.eventQueue = eventQueue;
        this.eventIdToObjectNode = eventIdToObjectNode;
        this.persistenceDrivers.addAll(persistenceDrivers);
        this.exceptionHandler = exceptionHandler;
    }

    public void stop() {
        log.debug("Stopping async processing of events");
        this.isRunning = false;
    }

    private void processEventNode(JsonNode eventNode) {
        JsonNode generatedFieldsNode = this.eventIdToObjectNode.get(
                        eventNode.get(Constants.EVENT_ID_FIELD_NAME).asText())
                .get(Constants.GENERATED_FIELDS_FIELD_NAME);
        processGeneratedFields(
                eventNode,
                Utils.objectMapper.convertValue(generatedFieldsNode, new TypeReference<>() {
                }));
        persist(eventNode);
    }

    private void persist(JsonNode eventNode) {
        for (PersistenceDriver driver : this.persistenceDrivers) {
            driver.persist(eventNode);
        }
    }

    private void processGeneratedFields(JsonNode event, Map<String, String> generatedFields) {
        if (generatedFields.isEmpty()) {
            return;
        }

        for (var entry : generatedFields.entrySet()) {
            String fieldContent = entry.getValue();
            var matches = templatePattern.matcher(fieldContent).results();
            for (var match : matches.collect(Collectors.toSet())) {
                String nameOfFieldToInject = match.group(1);
                JsonNode injectedField = event.get(nameOfFieldToInject);
                if (injectedField == null) {
                    log.error("Template requires injection of non-existing field: {}", fieldContent);
                    continue;
                }
                fieldContent = fieldContent.replaceAll(match.group(0), injectedField.asText());
            }
            ((ObjectNode) event).set(entry.getKey(), new TextNode(fieldContent));
        }
    }

    @Override
    public void run() {
        isRunning = true;
        try {
            while (isRunning) {
                JsonNode eventNode = eventQueue.poll(10, TimeUnit.SECONDS);
                if (eventNode == null) {
                    continue; // Queue is empty
                }
                processEventNode(eventNode);
            }
        } catch (Throwable throwable) {
            exceptionHandler.accept(throwable);
        }
    }
}
