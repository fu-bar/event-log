package elog.structures;

public class Constants {
    public static final String EVENT_ID_FIELD_NAME = "eventID";
    public static final String GENERATED_FIELDS_FIELD_NAME = "generatedFields";
    public static final String JSON_SCHEMA_FIELD_NAME = "jsonSchema";
}
