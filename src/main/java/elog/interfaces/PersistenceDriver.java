package elog.interfaces;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface PersistenceDriver {
    void persist(JsonNode eventNode);

    List<JsonNode> query(String query);
}
