package elog.handlers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;
import elog.interfaces.PersistenceDriver;
import elog.structures.Constants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.concurrent.BlockingQueue;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class EventHandler {

    private final BlockingQueue<JsonNode> eventQueue;
    private final Set<PersistenceDriver> persistenceDrivers = new HashSet<>();
    private final Map<String, JsonSchema> eventIdToJsonSchema = new HashMap<>();

    public EventHandler(Map<String, ObjectNode> eventIdToJsonNode, BlockingQueue<JsonNode> eventQueue, Collection<PersistenceDriver> persistenceDrivers) {
        this.eventQueue = eventQueue;
        this.persistenceDrivers.addAll(persistenceDrivers);
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V201909);
        for (var idToNode : eventIdToJsonNode.entrySet()) {
            eventIdToJsonSchema.put(idToNode.getKey(), factory.getSchema(idToNode.getValue().get(Constants.JSON_SCHEMA_FIELD_NAME)));
        }
    }

    @POST
    @Path("/event")
    public Response reportEvent(JsonNode event) {
        JsonNode eventIdNode = event.get(Constants.EVENT_ID_FIELD_NAME);
        if (eventIdNode == null || !this.eventIdToJsonSchema.containsKey(eventIdNode.asText())) {
            String errorMessage = String.format("No suitable schema found for %s=%s among %s", Constants.EVENT_ID_FIELD_NAME, eventIdNode == null ? "NULL" : eventIdNode.asText(), this.eventIdToJsonSchema.keySet());
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), errorMessage)
                    .entity(Map.of("error", errorMessage))
                    .build();
        }
        String eventId = eventIdNode.asText();
        JsonSchema jsonSchema = this.eventIdToJsonSchema.get(eventId);
        Set<ValidationMessage> errors = jsonSchema.validate(event);
        if (!errors.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), "Event schema validation failed")
                    .entity(errors)
                    .build();
        }

        this.eventQueue.add(event);
        return Response.accepted().build();
    }

    @GET
    @Path("/event")
    public Response query(@QueryParam("query") String query) {
        if (query == null) { // Not supplied
            return Response.status(Response.Status.BAD_REQUEST.getStatusCode(), "'query' parameter missing").build();
        }
        List<JsonNode> results = new ArrayList<>();
        for (var driver : this.persistenceDrivers) {
            results.addAll(driver.query(query));
        }
        return Response.ok(results).build();
    }

}
