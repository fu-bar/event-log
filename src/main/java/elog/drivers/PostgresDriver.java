package elog.drivers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import elog.App;
import elog.interfaces.PersistenceDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PostgresDriver implements PersistenceDriver {
    private static final String SQL_INSERT_COMMAND = "INSERT INTO log.events (event) VALUES (?::json)";
    private final Connection connection;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Logger log;

    public PostgresDriver(String host, String dbName, String user, String password) {
        Connection dbConnection;
        this.log = LoggerFactory.getLogger(PostgresDriver.class);
        String connectionString = String.format("jdbc:postgresql://%s/%s", host, dbName);
        log.debug("Connecting to DB: {}", connectionString);
        Properties properties = new Properties();
        properties.setProperty("user", user);
        properties.setProperty("password", password);
        properties.setProperty("ssl", "false");
        try {
            dbConnection = DriverManager.getConnection(connectionString, properties);
        } catch (SQLException e) {
            log.error("Failed connecting to Postgres", e);
            App.abort("Connection to Postgres failed", e);
            dbConnection = null;
        }
        this.connection = dbConnection;
        log.debug("Connected successfully");
    }

    @Override
    public void persist(JsonNode eventNode) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_COMMAND)) {
            preparedStatement.setString(1, eventNode.toPrettyString());
            preparedStatement.execute();
        } catch (SQLException e) {
            App.abort(e.getMessage(), e);
        }
    }

    @Override
    public List<JsonNode> query(String query) {
        List<JsonNode> results = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String event = resultSet.getString("event");
                results.add(objectMapper.readTree(event));
            }
            return results;
        } catch (SQLException | JsonProcessingException e) {
            log.error(e.getMessage(), e);
            return List.of(); // TODO: create query-exception
        }
    }
}
