package elog;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import elog.config.Config;
import elog.drivers.PostgresDriver;
import elog.handlers.EventHandler;
import elog.interfaces.PersistenceDriver;
import elog.structures.EventAsyncProcessor;
import elog.utils.Utils;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Consumer;


public class App extends Application<Config> implements Consumer<Throwable> {
    private static final Logger log;
    private Future<?> eventProcessingFuture;
    private EventAsyncProcessor eventAsyncProcessor;
    private final Set<PersistenceDriver> persistenceDrivers = new HashSet<>();
    private final BlockingQueue<JsonNode> eventQueue = new LinkedBlockingQueue<>();
    private final ExecutorService eventProcessingExecutor = Executors.newSingleThreadExecutor();


    static {
        log = LoggerFactory.getLogger(App.class);
    }

    public static void abort(String errorMessage, Throwable throwable) {
        if (throwable != null) {
            App.log.error("[Terminating] {}", errorMessage, throwable);
        } else {
            App.log.error("[Terminating] {}", errorMessage);
        }
        System.exit(1);
    }


    public static void main(String[] args) throws Exception {
        String configFilePath;
        if (args.length > 0) {
            configFilePath = args[0];
            log.debug("Configuration file is set to: {}", configFilePath);
        } else {
            configFilePath = "./config.yaml";
            log.warn("Configuration file was not supplied in command line, defaulting to: {}", configFilePath);
        }
        if (!Utils.fileExists(configFilePath)) {
            abort(String.format("Configuration file not found: %s", configFilePath), null);
        }
        Utils.printLogo();
        App app = new App();
        app.run("server", configFilePath);
    }

    @Override
    public void run(Config configuration, Environment environment) {
        Map<String, String> dbDetails = configuration.getDb();
        PostgresDriver postgresDriver = new PostgresDriver( // Currently, only supporting postgres consistency
                dbDetails.get("host"),
                dbDetails.get("dbName"),
                dbDetails.get("user"),
                dbDetails.get("password")
        );
        this.persistenceDrivers.add(postgresDriver);
        Map<String, ObjectNode> idToJsonNode = new HashMap<>();
        try {
            idToJsonNode = Utils.collectEventDetails(configuration.getEventsDetailsFolder());
            if (idToJsonNode.isEmpty()) {
                abort(String.format("No event details found in %s", configuration.getEventsDetailsFolder()), null);
            }
        } catch (IOException e) {
            abort(String.format("Failed reading: %s", configuration.getEventsDetailsFolder()), e);
        }
        EventHandler eventHandler = new EventHandler(idToJsonNode, eventQueue, persistenceDrivers);
        environment.jersey().register(eventHandler);
        Utils.registerDummyHealthCheck(environment);
        this.eventAsyncProcessor = new EventAsyncProcessor(this.eventQueue, idToJsonNode, persistenceDrivers, this);
        this.eventProcessingFuture = eventProcessingExecutor.submit(eventAsyncProcessor);
    }

    @Override
    public void accept(Throwable throwable) {
        eventAsyncProcessor.stop();
        this.eventProcessingFuture.cancel(true);
        abort(String.format("Unexpected exception: %s", throwable.getMessage()), throwable);
    }
}
